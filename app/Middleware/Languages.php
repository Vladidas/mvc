<?php

namespace App\Middleware;

use Vendor\Http\Request;
use Vendor\Languages\Translate;

class Languages
{
    public function handle(&$request)
    {
        // Get lang_id.
        $lang_id = in_array($request->get->arguments[0], $GLOBALS['LANGUAGES'])
            ? $request->get->arguments[0]
            : $GLOBALS['LANGUAGES'][0];

        // Set locale lang in site.
        Translate::$lang_id = $lang_id;

        return $request;
    }
}