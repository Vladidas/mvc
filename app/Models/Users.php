<?php

namespace App\Models;

use Vendor\DB\Models;

class Users extends Models
{
    public $table_name = 'clients';
    public $connection = 'mysql';

    public function role()
    {
        return $this->hasMany(Roles::class, 'name_id', 'id');
    }

    public function abc()
    {
        return $this->hasMany(Roles::class, 'id', 'manager_id')->select('id', 'last_name')->where('name', 'Vlad');
    }
}