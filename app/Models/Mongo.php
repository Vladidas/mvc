<?php

namespace App\Models;

use Vendor\DB\Models;

class Mongo extends Models
{
    public $database = 'clients_db';
    public $collection = 'clients_tbl';

    public $connection = 'mongodb';
}