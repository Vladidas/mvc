<?php

namespace App\Controllers\errors;

use App\Controllers\Controller;

class ErrorController extends Controller
{
    /** Show 404 error page. */
    public function error404()
    {
        return views('errors/404', []);
    }
}