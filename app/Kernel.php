<?php

return [
    // Middleware.
    'middleware' => [
        'web' => [
            \App\Middleware\Web::class,
        ],
        'lang' => \App\Middleware\Languages::class,
    ],

    // Drivers DB.
    'db' => [
        'mysql'=> \Vendor\DB\drivers\MySQL\MySQL::class,
        'mongodb'=> \Vendor\DB\drivers\MongoDB\MongoDB::class,
    ]
];