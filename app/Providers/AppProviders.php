<?php

namespace App\Providers;

use Vendor\Languages\Translate;

class AppProviders
{
    /** Handle all variables and send to views. */
    public function handle()
    {
        return [
            'lang_id' => Translate::$lang_id,
            'url' => \Route::$url,
            'route_name' => \Route::$route_name,
//            'translate' => Vendor\Translate\Translate::getFile(),
        ];
    }
}