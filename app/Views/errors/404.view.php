<!DOCTYPE html>
<html lang="<?php echo $lang_id ?>">
<head>

	<meta charset="utf-8">

	<title>Frankof</title>
	<meta name="description" content="">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<meta property="og:image" content="path/to/image.jpg">
	<link rel="shortcut icon" href="<?php path_public('/img/favicon/frankof-favicon.ico') ?>" type="image/x-icon">
	<link rel="frankof-icon" href="<?php path_public('img/favicon/frankof-favicon.ico') ?>">
	<link rel="frankof-icon" sizes="72x72" href="<?php path_public('img/favicon/frankof-favicon.ico') ?>">
	<link rel="frankof-icon" sizes="114x114" href="<?php path_public('img/favicon/frankof-favicon.ico') ?>">

	<style>
		body {
			opacity: 0;
			overflow-x: hidden;
		}

		html {
			background-color: #333;
		}
	</style>

</head>
    <body>
	    <div class="frankof-error">
			<p class="error-text">Помилка!
				<span></span>
				<span></span>
				<span></span>
			</p>
			<p class="error-numbers">404
				<svg class="error-lamp" xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink"
					width="145px" height="381px">
					<path fill-rule="evenodd"  fill="rgb(72, 72, 72)"
					d="M38.668,360.969 C47.789,356.814 65.337,354.000 85.497,354.000 C105.656,354.000 123.204,356.814 132.325,360.969 L139.000,360.969 L139.000,367.500 C139.000,374.956 115.046,381.000 85.497,381.000 C55.947,381.000 31.994,374.956 31.994,367.500 L31.994,360.969 L38.668,360.969 Z"/>
					<path fill-rule="evenodd"  fill="rgb(123, 123, 123)"
					d="M116.322,146.089 C102.934,202.576 99.324,274.845 99.057,346.711 C122.388,348.061 139.657,352.899 139.657,358.663 C139.657,365.473 115.558,370.992 85.832,370.992 C56.105,370.992 32.007,365.473 32.007,358.663 C32.007,351.854 56.105,346.335 85.832,346.335 C87.462,346.335 89.072,346.354 90.666,346.387 C90.948,273.843 94.664,201.245 108.118,144.476 C120.479,92.318 141.588,53.856 136.105,35.582 C129.403,13.246 109.206,7.111 96.214,7.692 C84.531,8.214 74.624,14.476 69.034,24.454 L70.318,24.891 C74.039,26.155 76.032,30.197 74.765,33.917 L67.521,55.211 C72.503,59.328 77.656,66.115 79.861,77.208 C83.871,97.377 77.532,112.194 77.532,112.194 L38.604,99.495 L0.001,85.841 C0.001,85.841 4.013,70.233 19.491,56.688 C28.385,48.906 36.967,46.804 43.547,46.747 L50.694,25.736 C51.960,22.015 56.003,20.025 59.724,21.290 L61.108,21.760 C67.946,8.862 80.673,0.735 95.801,0.058 C111.685,-0.655 135.699,6.595 143.879,32.705 C150.004,52.251 129.482,90.553 116.322,146.089 Z"/>
					<path fill-rule="evenodd"  fill="rgb(148, 148, 148)"
					d="M1.032,85.741 C0.209,88.225 16.545,96.178 37.518,103.505 C58.491,110.831 76.160,114.756 76.982,112.272 C77.805,109.788 61.470,101.834 40.496,94.508 C19.523,87.181 1.854,83.257 1.032,85.741 Z"/>
					<path fill-rule="evenodd"  fill="rgb(86, 86, 86)"
					d="M50.882,98.145 C48.282,102.702 42.881,104.908 37.856,103.135 C32.831,101.363 29.845,96.200 30.464,90.944 L50.882,98.145 Z"/>
				</svg>
				<svg class="error-lamp-light light" xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink"
					width="638px" height="388px">
					<defs>
					<radialGradient id="PSgrad_0">
					<stop offset="0%" stop-color="rgb(255,255,255)" stop-opacity="1" />
					<stop offset="100%" stop-color="rgb(255,255,255)" stop-opacity="1" />
					</radialGradient>
					</defs>
					<path fill-rule="evenodd"  opacity="0.231" fill="url(#PSgrad_0)"
					d="M380.000,388.000 L560.000,3.000 L638.000,388.000 L380.000,388.000 ZM-0.000,388.000 L557.000,2.000 L321.000,388.000 L-0.000,388.000 ZM-0.000,124.000 L553.000,-0.000 L-0.000,327.000 L-0.000,124.000 Z"/>
				</svg>
				<svg class="error-lamp-light full-light" xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink"
					width="638px" height="388px">
					<defs>
					<radialGradient id="PSgrad_0">
					<stop offset="0%" stop-color="rgb(255,255,255)" stop-opacity="1" />
					<stop offset="100%" stop-color="rgb(255,255,255)" stop-opacity="1" />
					</radialGradient>

					</defs>
					<path fill-rule="evenodd"  opacity="0.231" fill="url(#PSgrad_0)"
					d="M-0.000,388.000 L-0.000,123.000 L553.000,-0.000 L560.000,2.000 L638.000,388.000 L-0.000,388.000 Z"/>
				</svg>
			</p>
			<p class="error-text">сторінку не знайдено</p>
		</div>

		<link rel="stylesheet" href="<?php path_public('src/css/main.min.css') ?>">
		<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	</body>
</html>
