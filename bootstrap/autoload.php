<?php

// If not isset session start session.
if(!isset($_SESSION)) session_start();

// Include config.
foreach(glob('config/*.php') as $file) {
    require_once $file;
}

// Include base autoloader.
require_once __DIR__ . '/../vendor/autoload.php';

// Include base parent routing.
require_once __DIR__ . '/../vendor/functions.php';