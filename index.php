<?php

// Include autoload kernel.
require_once __DIR__ . '/bootstrap/autoload.php';

// Include base parent routing.
require_once __DIR__ . '/vendor/Http/Route.php';

// Include and start routing.
require_once __DIR__ . '/http/router.php';