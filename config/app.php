<?php

/************************
 ***** USERS CONFIG *****
 ************************/

/** SECURITY. */
// Set anti csrf-attack.
define('CSRF_TOKEN', '601d4e8b5fd64071b944e774a0dfe26a');

/** LANGUAGES. */
$GLOBALS['LANGUAGES'] = ['ua', 'en', 'ru'];

/** PATH. */
// Path to public.
define('PATH_PUBLIC', '/App/Public/');
define('PATH_VIEW', 'App/Views/');

/** DB. */
// Const from connect to MySQL.
define('DB_MYSQL_LOCALHOST', 'localhost');
define('DB_MYSQL_DATABASE',  'printing_db_full');
define('DB_MYSQL_USERNAME',  'root');
define('DB_MYSQL_PASSWORD',  'mysql');
define('DB_MYSQL_PORT',      '3306');
define('DB_MYSQL_CHARSET',   'utf8');

// Const from connect to MySQL.
define('DB_MONGODB_LOCALHOST', 'mongodb://localhost:27017');
define('DB_MONGODB_DATABASE',  'test');

/** SEND SERVICES. */
// Recipient SMTP E-Mail.
define('EMAIL_RECIPIENT', 'vlad.golubtsov@ukr.net');
define('EMAIL_HEADER',    'Drukarka Робот');
define('EMAIL_LOGIN',     'robot@drukarka.com.ua');
define('EMAIL_PASSWORD',  '62Pam4DB1Bpr');