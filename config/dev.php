<?php

/************************
 ***** DEVELOPERS CONFIG *****
 ************************/

/** ERRORS. */
// Display errors from application.
ini_set('display_errors', true);

// Declare error reporting levels.
ini_set('error_reporting', E_ALL & ~E_NOTICE);

// Declare file to save errors logs.
ini_set('error_log', 'Storage/logs/errors.log'); // or disable string if not saved to file.

// Declare file error logs.
define('ERRORS_LOGS_EXCEPTIONS_ENABLE', true);
define('ERRORS_LOGS_SHOW_ENABLE', true);
define('ERRORS_LOGS_FILE_ENABLE', true);

/** KERNEL. */
// Declare using Drivers.
define('DB_DRIVER', 'mongodb'); // 'mysql', 'mongodb'

// Declare kernel config.
define('KERNEL', require_once __DIR__ . '/../app/Kernel.php');

/** BUILD PASSWORD. */
//password_hash('password', PASSWORD_BCRYPT, [12])