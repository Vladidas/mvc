# PHP MVC Micro-Framework by [Vladidasovi4](https://www.linkedin.com/in/vlad-golubtsov-43074b150)

Faster, responsible and usability MVC micro-framework.

## Official Documentation

TODO... His will been later :)

## Install:

`composer require vladidas/mvc @dev`

## Using:

1. #### Routing:
___file: `HTTP\router.php`___ 

```php
// Languages.
Route::lang(['ua', 'end', 'ru']);

// GET-rquests.
Route::get('/', 'client/IndexController@index');
Route::get('/', 'client/IndexController@index', ['name' => 'home']);

// POST-requests.
Route::post('/', 'client/IndexController@index');
Route::post('/', 'client/IndexController@index', ['name' => 'home']);

// Error exceptions.
Route::error('404', 'errors/ErrorController@error404');
```

1. #### Models:
___folder: `App\Models`___ 

There create all models, whict has been using in application.
- #### Configs:
```php

class Users extends Models
{
    public $table_name = 'clients';
    public $connection = 'mysql';

    //
}
```

- #### Relationships:

- hasOne:
```php
public function role()
{
    return $this->hasOne(Roles::class, 'id', 'manager_id');
}

// or

public function role()
{
    return $this->hasOne('App\Models\Roles', 'id', 'manager_id')->orderBy('id', 'desc');
}

// or

public function role()
{
    return $this->hasOne(Roles::class, 'id', 'manager_id')->select('id', 'name')->where('id', 1);
}
```

- hasMany:
```php
public function users()
{
    return $this->hasMany(Users::class, 'id', 'manager_id');
}

// or

public function users()
{
    return $this->hasMany('App\Models\Users', 'id', 'manager_id')->orderBy('id', 'desc');
}

// or

public function users()
{
    return $this->hasMany(Users::class, 'id', 'manager_id')->select('id', 'name')->where('id', '>', 1);
}
```

2. #### Controllers:
___folder: `App\Controllers`___ 

```php
<?php

namespace App\Controllers\client;

use App\Controllers\Controller;
use Vendor\Http\Request;
use App\Models\Users;

class IndexController extends Controller
{
    /** Show all users. */
    public function index()
    {
        $users = Users::with('role')
            ->where('id', '>', 1)
            ->orderBy('id', 'desc')
            ->get();

        return views('users/index', compact($users));
    }

    /** Show user. */
    public function show(Request $request)
    {
        $users = Users::with('role')
            ->select('id', 'name')
            ->where('id', '>', $request->get->arguments->id)
            ->get();

        return views('users/show', [
            'users' => $users
        ]);
    }
}
```

2. #### Views:
___folder: `App\Views`___ 

```
<html lang="<?= $lang_id ?>">
    <head>
        <meta charset="utf-8">
        <title><?= $title ?></title> <!-- Example use varialbe in view. -->
        <link rel="stylesheet" type="text/css" href="<?php path_public('src/css/main.css') ?>"> <!-- Include style. -->
    </head>
    
    <?php require_once path_view('include/menu') ?> <!-- Include view. -->
    
    <body>
        <form action="/" method="post">
            <?php csrf_field() ?> <!-- Use CSRF-validate. -->
            
            <input type="text" name="name" value="<?= $name ?>">
            <input type="submit" value="Тиць">
        </form>
    </body>
</html>
```