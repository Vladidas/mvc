<?php

/** Debugger. */
function dump(...$array)
{
    dumpBuilding($array);
}

/** Debugger and die.*/
function dd(...$array)
{
    dumpBuilding($array);
    die;
}

/** Build arrays for debug.*/
function dumpBuilding($array)
{
    echo '<pre style="color: greenyellow; background: black; display: inline-block">';

    if(is_array($array) or is_object($array)) {
        foreach ($array as $value) {
            var_dump($value);
        }
    } else {
        var_dump($array);
    }

    echo '</pre><br>';
}

/** Paste CSRF-token.*/
function csrf_token()
{
    echo CSRF_TOKEN;
}

/** Paste CSRF-token field.*/
function csrf_field()
{
    echo '<input type="hidden" name="_csrf-token" value="'. CSRF_TOKEN .'">';
}

/** Send path to view.*/
function path_view($path)
{
    return PATH_VIEW . $path . '.view.php';
}

/** Send path to public.*/
function path_public($path)
{
    echo PATH_PUBLIC . $path;
}

/** Declare to call method for open view in controller. */
function views($filename, $data = null)
{
    // Send to view variables.
    $providers = new \App\Providers\AppProviders();
    extract($providers->handle());

    // Extract variables.
    if($data) {
        extract($data);
    }

    // Get file.
    $file = __DIR__ . "/../App/Views/{$filename}.view.php";

    // Incude view.
    if(file_exists ($file)) {
        return require_once $file;
    }

    return false;
}

/** Send errors to exceptions handler. */
if(ERRORS_LOGS_EXCEPTIONS_ENABLE) {
    set_exception_handler(function($exception) {
       \Vendor\Errors\Error::realizeErrors($exception);
    });
}