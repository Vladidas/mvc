<?php

namespace Vendor\DB;

interface Drivers
{
    public function from($args);


    public function select($args);

    public function distinct();


    public function where($args);

    public function orWhere($args);


    public function whereIn($args);

    public function orWhereIn($args);

    public function whereNotIn($args);

    public function orWhereNotIn($args);


    public function whereNull($args);

    public function orWhereNull($args);

    public function whereNotNull($args);

    public function orWhereNotNull($args);


    public function whereExists($args);

    public function orWhereExists($args);

    public function whereNotExists($args);

    public function orWhereNotExists($args);


//    public function having($args);

//    public function havingRaw($args);


//    public function join($args);
//
//    public function leftJoin($args);
//
//    public function rightJoin($args);
//
//    public function fullOuterJoin($args);


    public function orderBy($args);

//    public function groupBy($args);

    public function with($args);


    public function hasOne($args);

    public function hasMany($args);


    public function get();
}