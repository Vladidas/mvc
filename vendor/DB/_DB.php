<?php

class DB
{
    public $table_name;
    public $mysqli;
    public $sql = "";
    public $sql_columns = "";
    public $with = [];
    public $with_query_first_table = false;

    /**
     * DB destructor - open, send parameters and connection from DB.
     * @param $table_name
     */
    public function __construct()
    {
        $this->mysqli = new mysqli(
            DB_LOCALHOST,
            DB_USERNAME,
            DB_PASSWORD,
            DB_DATABASE
        );

        // Set charset to db.
        $this->mysqli->set_charset(DB_CHARSET);
    }

    /**
     * Set table name for operation;
     * @param null $arg
     * @return $this object
     */
    public function table($table_name)
    {
        $this->table_name = $table_name;
        return $this;
    }

    /**
     * Selected from table data.
     * @param string|array $columns - get margins from DB
     * @return $this SQL
     */
    public function select($columns = '*')
    {
        if(is_array($columns)) {
            foreach ($columns as $col) {
                $this->sql_columns .= $col . ', ';
            }
            $this->sql_columns = substr($this->sql_columns, 0, -2);
        } else {
            $this->sql_columns = $columns;
        }

        $this->sql .= "SELECT {$this->sql_columns} FROM {$this->table_name} ";
        return $this;
    }

    /**
     * Concat to SQL query string.
     * @param string|array $columns - get margins from DB
     * @return $this SQL
     */
    public function concatSQL($string = '')
    {
        $this->sql .= " $string ";
        return $this;
    }

    /**
     * OrderBy in table.
     * @param string|array $columns - get margins from DB
     * @return $this SQL
     */
    public function orderBy($field, $sort = 'ASC')
    {
        $this->sql .= " ORDER BY {$field} {$sort} ";
        return $this;
    }

    /**
     * Set limits selected SQL.
     * @param string|array $columns - get margins from DB
     * @return $this SQL
     */
    public function limit($count = null)
    {
        if(is_int($count)) {
            $this->sql .= " LIMIT {$count} ";
        }

        return $this;
    }

    /**
     * Search refinement SQL request.
     * @param $arguments - arguments from search to DB
     * @return $this SQL
     */
    public function where($arguments)
    {
        $this->sql .= (strpos($this->sql, 'WHERE') === false ? ' WHERE' : ' AND' ) . " {$arguments} ";
        return $this;
    }

    /**
     * Search refinement SQL request array.
     * @param $arguments - arguments from search to DB
     * @return $this SQL
     */
    public function whereIn($arguments, $array)
    {
        $this->sql .= (strpos($this->sql, 'WHERE') === false ? ' WHERE' : ' AND' ) . " {$arguments} IN (" . implode(',', $array) . ")";
        return $this;
    }

    /**
     * Get only left join selected from DB.
     * @param null $keyBy - key from associative array.
     * @return array
     */
    public function leftJoin($table2, $table1_arg, $table2_arg)
    {
        $this->sql .= "LEFT OUTER JOIN {$table2} ON {$this->table_name}.{$table1_arg}={$table2}.{$table2_arg} ";
        return $this;
    }

    /**
     * This method is relationship between 2 tables, One-To-One.
     * @param $table2
     * @param $table1_arg
     * @param $table2_arg
     * @param $key_by_table1
     * @return array result associative array.
     */
    public function hasOne($table2, $table1_arg, $table2_arg, $key_by_table1)
    {
        $query_table1 = $this->mysqli->query("SELECT * FROM {$this->table_name} " . $this->sql);
        $query_table2 = $this->mysqli->query("SELECT * FROM {$table2}");

        // Get all records from table2 and get key this arrays table2_args.
        $result_table2 = [];
        while ($row_table2 = $query_table2->fetch_assoc()) {
            $result_table2[$row_table2[$table2_arg]] = $row_table2;
        }

        // Get all records from table1 and get key this arrays table1_args.
        $result_table1 = [];
        while ($row_table1 = $query_table1->fetch_assoc()) {
            $result_table1[$row_table1[$key_by_table1]] = $row_table1;
            $result_table1[$row_table1[$key_by_table1]][$table2] = $result_table2[$row_table1[$table1_arg]];
        }

        return $result_table1;
    }

    /**
     * Get relation table and push hit to parent.
     * @param $table2
     * @param $table1_arg
     * @param $table2_arg
     * @return $this.
     */
    public function with($this_table, $first_table_arg, $this_table_arg)
    {
        // Get first parent table if not isset him.
        if(!$this->with_query_first_table){
            $this->with_query_first_table = $this->mysqli->query("SELECT * FROM {$this->table_name} " . $this->sql);

            // Get all records from parent table.
            while ($first_row_table = $this->with_query_first_table->fetch_assoc()) {
                $this->with[] = $first_row_table;
            }
        }

        // Get this table for has many relation.
        $this_query_table = $this->mysqli->query("SELECT * FROM {$this_table}");

        while ($this_row_table = $this_query_table->fetch_assoc()) {
            foreach ($this->with as $key => $value) {
                if (isset($value[$first_table_arg]) && $value[$first_table_arg] == $this_row_table[$this_table_arg]) {
                    $this->with[$key][$first_table_arg] = $this_row_table;
                }
            }
        }

        return $this;
    }

    /**
     * Get all with and push to global array.
     * @param $table2
     * @param $table1_arg
     * @param $table2_arg
     * @return $this.
     */
    public function getWith()
    {
        return $this->with;
    }

    /**
     * This method is relationship between 2 tables, One-To-Many.
     * @param $table2
     * @param $table1_arg
     * @param $table2_arg
     * @param $key_by_table1
     * @return array result associative array.
     */
    public function hasMany($table2, $table1_arg, $table2_arg, $key_by_table1)
    {
        // Select all rows from DB.
        $query_table1 = $this->mysqli->query("SELECT * FROM {$this->table_name} " . $this->sql);
        $query_table2 = $this->mysqli->query("SELECT * FROM {$table2}");

        // Get all records from table2 and get key this arrays table2_args.
        $result_table2 = [];
        while ($row_table2 = $query_table2->fetch_assoc()) {
            $result_table2[$row_table2[$table2_arg]][] = $row_table2;

        }

        // Get all records from table1 and get key this arrays table1_args.
        $result_table1 = [];
        while ($row_table1 = $query_table1->fetch_assoc()) {
            $result_table1[$row_table1[$key_by_table1]] = $row_table1;
            $result_table1[$row_table1[$key_by_table1]][$table2] = $result_table2[$row_table1[$table1_arg]];
        }

        return $result_table1;
    }

    /**
     * This method is create new entry if not exists from DB and vice versa
     * @param $set
     * @return $this SQL
     */
    public function createOrUpdate($type, $array)
    {
        // Array to string convertions.
        $fields = implode(", ", array_keys($array));

        // Fields name to ? replaced.
        $replaced_fields = preg_replace('/\w+/', '?', $fields);

        $insert = $this->mysqli->prepare("REPLACE INTO {$this->table_name} ({$fields}) VALUES ({$replaced_fields}) ");
        call_user_func_array([$insert, 'bind_param'], $this->bindParamsForInsert(array_merge([$type], $array)));

        return $insert->execute() ? $insert->insert_id : false;
    }

    /**
     * Inserting to DB data prepare methods.
     * @param $array = [
     *  [], [], []
     * ]
     * @return [id record]|[false]
     */
    public function insert($type, $array)
    {
        // Array to string convertions.
        $fields = implode(", ", array_keys($array));

        // Fields name to ? replaced.
        $replaced_fields = preg_replace('/\w+/', '?', $fields);

        // Prepare SQL query.
        $insert = $this->mysqli->prepare("INSERT INTO {$this->table_name} ({$fields}) VALUES ({$replaced_fields})");
        call_user_func_array([$insert, 'bind_param'], $this->bindParamsForInsert(array_merge([$type], $array)));

        return $insert->execute() ? $insert->insert_id : false;
    }

    /**
     * Special method for inserting data.
     * @param $arr
     * @return array
     */
    private function bindParamsForInsert($arr){
        $refs = [];
        foreach($arr as $key => $value) {
            $refs[$key] = &$arr[$key];
        }
        return $refs;
    }

    /**
     * Delete recordings.
     * @return $this
     */
    public function delete()
    {
        return $this->mysqli->query("DELETE FROM {$this->table_name} " . $this->sql);
    }

    /**
     * Upate entry from DB.
     * @param array $set
     * @return boolean result
     */
    public function update($type, $array)
    {
        // Array to string convertions.
        $fields = implode('=?, ', array_keys($array)) . '=?';

        // Prepare SQL query.
        $update = $this->mysqli->prepare("UPDATE {$this->table_name} SET {$fields} " . $this->sql);
        call_user_func_array([$update, 'bind_param'], $this->bindParamsForInsert(array_merge([$type], $array)));

        return $update->execute() ? $update->insert_id : false;
    }

    /**
     * Processing and get all data from DB
     * @param null $keyBy - key from associative array.
     * @return array
     */
    public function get($keyBy = null)
    {
        $query = $this->mysqli->query($this->sql);

        $result = [];
        $i = 0;

        if($query) {
            while ($row = $query->fetch_assoc()) {
                $result[!$keyBy ? $i++ : $row[$keyBy]] = $row;
            }
        }

        return $result;
    }

    /**
     * Processing and get only first data from DB.
     * @param null $keyBy - key from associative array.
     * @return array
     */
    public function first($key = null)
    {
        $query = $this->mysqli->query($this->sql);

        return $query
            ? ($key
                ? $query->fetch_assoc()[$key]
                : $query->fetch_assoc())
            : false;
    }

    /**
     * Set from DB inserting methods.
     * @return bool|mysqli_result
     */
    public function query()
    {
        $query = $this->mysqli->query($this->sql);
        return $query;
    }

    /**
     * DB destructor - close connection from DB.
     * @param $table_name
     */
    public function __destruct()
    {
        $this->mysqli->close();
    }
}