<?php

namespace Vendor\DB\Drivers\MySQL;

use Vendor\DB\Models;
use Vendor\DB\Drivers;
use Vendor\DB\drivers\MySQL\Where;

class MySQL implements Drivers
{
    /** Declare data about DB. */
    private $mysqli;

    /** Declare global array about model. */
    public $model;

    private static $relation;

    /** Declare as @singleton result array, when collect all data about operations. */
    public static $result_query;

    /** Declare relationship table name. */
    /** Open, send parameters and connection from DB/ */
    public function __construct($model)
    {
        // Declare all objects from driver.
        $this->model = $model;

        $this->connect();

        $this->declareResultQuery();
    }

    /** Get model class. */
    private function getModelClass()
    {
        return new Where($this);
    }

    /** Declare result array existing queries. */
    private function declareResultQuery()
    {
        self::$result_query = self::$result_query ?: new \stdClass();
    }

    /** For relationship push to one type key result query, for main query - another. */
    public function pushToQuery()
    {
        return self::$relation
            ? self::$result_query->relations->{self::$relation}
            : self::$result_query;
    }

    /** Connect to DB. */
    private function connect()
    {
        $this->mysqli = mysqli_connect(
            DB_MYSQL_LOCALHOST,
            DB_MYSQL_USERNAME,
            DB_MYSQL_PASSWORD,
            DB_MYSQL_DATABASE,
            DB_MYSQL_PORT
        );

        $this->mysqli->set_charset(DB_MYSQL_CHARSET);
    }

    /** Get table name active relation, which use now. */
    private function getTableNameRelations($model_relationship)
    {
        return (new Models)->getTableName($model_relationship, (new $model_relationship)->table_name);
    }

    /** Push to result query info about relationships. */
    private function pushRelations($args, $method_name)
    {
        // Delcare variables relationship.
        $model_relationship = $args[0] ?: false;

        // Push to result array data about relationship.
        if($model_relationship) {
            $this->pushToQuery()->from = $this->getTableNameRelations($model_relationship);
            $this->pushToQuery()->method = $method_name;
            $this->pushToQuery()->child_table_key = $args[1] ?: 'id';
            $this->pushToQuery()->foreign_key = $args[2] ?: 'id';
        }
    }

    /** Change table name. */
    public function from($args)
    {
        $args = array_shift($args);

        if(is_string($args)) {
            $this->model->table_name = trim($args);
        }
    }

    /** Push to result query SELECT part. */
    public function select($args)
    {
        // Get arguments.
        if(is_array($args[0])) {
            $arguments = implode(', ', $args[0]);
        } else {
            $arguments = implode(', ', $args);
        }

        // Push to result query.
        if(!empty($arguments)) {
            $this->pushToQuery()->select .= ($this->pushToQuery()->select ? ', ' : '') . $arguments ?: '';
        }
    }

    /** Push to result query SELECT DISTINCT part. */
    public function distinct()
    {
        if(!empty($this->pushToQuery()->select)) {
            $this->pushToQuery()->select = 'DISTINCT ' . $this->pushToQuery()->select;
        }
    }

    /** Declare "WHERE / AND WHERE" method for operation in DB. */
    public function where($args)
    {
        $this->getModelClass()->buildWhereConditions($args, 'AND');
    }

    /** Declare "WHERE / OR WHERE" method for operation in DB. */
    public function orWhere($args)
    {
        $this->getModelClass()->buildWhereConditions($args, 'OR');
    }

    /** Declare "WHERE IN" method from operation in DB. */
    public function whereIn($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' AND ' : '') . $this->getModelClass()->buildWhereInConditions($args);
    }

    /** Declare "OR WHERE IN" method from operation in DB. */
    public function orWhereIn($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' OR ' : '') . $this->getModelClass()->buildWhereInConditions($args);
    }

    /** Declare "WHERE NOT IN" method from operation in DB. */
    public function whereNotIn($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' AND ' : '') . $this->getModelClass()->buildWhereNotInConditions($args);
    }

    /** Declare "OR WHERE NOT IN" method from operation in DB. */
    public function orWhereNotIn($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' OR ' : '') . $this->getModelClass()->buildWhereNotInConditions($args);
    }

    /** Declare WHERE EXISTS(conditions)" method from operation in DB. */
    public function whereExists($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' AND ' : '') . $this->getModelClass()->buildWhereExistsConditions($args);
    }

    /** Declare OR WHERE EXISTS(conditions)" method from operation in DB. */
    public function orWhereExists($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' OR ' : '') . $this->getModelClass()->buildWhereExistsConditions($args);
    }

    /** Declare WHERE NOT EXISTS(conditions)" method from operation in DB. */
    public function whereNotExists($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' AND ' : '') . $this->getModelClass()->buildWhereNotExistsConditions($args);
    }

    /** Declare OR WHERE NOT EXISTS(conditions)" method from operation in DB. */
    public function orWhereNotExists($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' OR ' : '') . $this->getModelClass()->buildWhereNotExistsConditions($args);
    }

    /** Declare WHERE (conditions) IS NULL" method from operation in DB. */
    public function whereNull($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' AND ' : '') . $this->getModelClass()->buildWhereNullConditions($args);
    }

    /** Declare OR WHERE (conditions) IS NULL" method from operation in DB. */
    public function orWhereNull($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' OR ' : '') . $this->getModelClass()->buildWhereNullConditions($args);
    }

    /** Declare WHERE (conditions) IS NOT NULL" method from operation in DB. */
    public function whereNotNull($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' AND ' : '') . $this->getModelClass()->buildWhereNotNullConditions($args);
    }

    /** Declare OR WHERE (conditions) IS NOT NULL" method from operation in DB. */
    public function orWhereNotNull($args)
    {
        $this->pushToQuery()->where .= ($this->pushToQuery()->where ? ' OR ' : '') . $this->getModelClass()->buildWhereNotNullConditions($args);
    }

    /** Declare "ORDER BY" method for operation in DB. */
    public function orderBy($args)
    {
        // Get all arguments.
        $args = array_shift($args);

        // Push to result query.
        switch (count($args)) {
            case 1: $this->pushToQuery()->orderBy = "{$args[0]} ASC"; break;
            case 2: $this->pushToQuery()->orderBy = "{$args[0]} {$args[1]}"; break;
        }
    }

    /** Get all relationship which having this model. */
    public function with($relations)
    {
        if(!empty($relations)) {
            self::$result_query->relations = new \stdClass();

            foreach ($relations as self::$relation) {
                // Create facade relationships in the result query array.
                self::$result_query->relations->{self::$relation} = new \stdClass();

                // Fulfill all relationships.
                $model = new $this->model->model_name;
                $model->{self::$relation}();
            }
        }

        // Clear relationships.
        self::$relation = null;
    }

    /** Declare method which join active table with one row another table. */
    public function hasOne($args)
    {
        $this->pushRelations($args, __FUNCTION__);
    }

    /** Declare method which join active table with many rows another table. */
    public function hasMany($args)
    {
        $this->pushRelations($args, __FUNCTION__);
    }

    /** Get result SQL request. */
    public function toSql($result_query = null)
    {
        // Get active query.
        $result_query = $result_query ?: self::$result_query;

        // Get select query.
        $select = 'SELECT ' . ($result_query->select ?: '*');

        // Get where query.
        $where = $result_query->where ? 'WHERE ' . $result_query->where : '';

        // Get from table.
        $from = 'FROM ' . ($result_query->from ?: $this->model->table_name);

        $query = "$select $from $where";

        return $query;
    }

    public function get($key_by = null)
    {
//        dump(self::$result_query);

        // Get result array.
        $result = $this->getRows($this->toSql(self::$result_query));

        foreach (self::$result_query->relations as $name => $args) {
            switch ($args->method) {
                case 'hasOne':
                    break;

                case 'hasMany':
                    // Get parents keys for the getting in child table with relationships.
                    $parents_table_keys = $this->getParentsTableKeys($result, $args->foreign_key);

                    // Push all keys which find in parent table for the finding in children table.
                    self::$relation = $name;
                    $this->whereIn([$args->child_table_key, $parents_table_keys]);

                    // Get data in child table.
                    array_push($result, $this->getRowsRelations($this->toSql($args), $args->from, $args->child_table_key));
                    break;
            }
        }

        return $result;
    }

    /** Get parents keys for the getting in child table with relationships. */
    private function getParentsTableKeys($result_array, $foreign_key)
    {
        return array_map(function ($parent) use ($foreign_key) {
            return $parent[$foreign_key];
        }, $result_array);
    }

    /** Send request to DB and get once row. */
    private function getRow($args, $key_by = null)
    {
        $query = $this->mysqli->query($args);

        $i = 0;
        $result = [];
        if($query) {
            while ($row = $query->fetch_assoc()) {
                $result[!$key_by ? $i++ : $row[$key_by]] = $row;
            }
        }

        return $result;
    }

    /** Send request to DB and get result. */
    private function getRows($args, $key_by = null)
    {
        $query = $this->mysqli->query($args);

        $i = 0;
        $result = [];
        if($query) {
            while ($row = $query->fetch_assoc()) {
                $result[!$key_by ? $i++ : $row[$key_by]] = $row;
            }
        }

        return $result;
    }

    /** Send request to DB and get result. */
    private function getRowsRelations($args, $table_name, $key_by)
    {
        $query = $this->mysqli->query($args);

        $i = 0;
        $result = [];
        if($query) {
            while ($row = $query->fetch_assoc()) {
                $result[!$key_by ? $i++ : $row[$key_by]][$table_name][] = $row;
            }
        }

        return $result;
    }
}