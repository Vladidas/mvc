<?php

namespace Vendor\DB\Drivers\MySQL;

use Vendor\DB\Models;

class Where
{
    /** Declare parent $this object. */
    private $mysql;

    public function __construct($mysql)
    {
        $this->mysql = $mysql;
    }

    /** Build 'WHERE / OR WHERE / GROUP WHERE' all conditions. */
    public function buildWhereConditions($args, $separator)
    {
        // Push to conditions.
        $where = &$this->mysql->pushToQuery()->where;

        // If it is group function.
        switch(count($args)) {
            case 1: // Recurse request.
                $args = array_shift($args);

                $where .= $this->concatConditions($where, $separator) . "(";
                if (is_callable($args)) $args(new Models());
                $where .= ")";
                break;

            case 2: // Call child group conditions methods having 2-arguments.
                $where .= $this->concatConditions($where, $separator) . "$args[0] = $args[1]";
                break;

            case 3: // Call child group conditions methods having 3-arguments.
                $where .= $this->concatConditions($where, $separator) . "$args[0] $args[1] $args[2]";
                break;
        }
    }

    /** Build 'WHERE IN / OR WHERE IN' all conditions. */
    public function buildWhereInConditions($args)
    {
        list($key, $value) = $args;

        if(is_array($value) && !empty($key)) {
            return "$key IN (" . implode(', ', $value) . ')';
        }
    }

    /** Build 'WHERE NOT IN / OR WHERE NOT IN' all conditions. */
    public function buildWhereNotInConditions($args)
    {
        list($key, $value) = $args;

        if(is_array($value) && !empty($key)) {
            return "$key NOT IN (" . implode(', ', $value) . ')';
        }
    }

    /** Build 'WHERE EXISTS / OR WHERE EXISTS' all conditions. */
    public function buildWhereExistsConditions($condition)
    {
        $condition = array_shift($condition);

        if(is_string($condition) && !empty(trim($condition))) {
            return "EXISTS (" . $condition . ')';
        }
    }

    /** Build 'WHERE NOT EXISTS / OR WHERE NOT EXISTS' all conditions. */
    public function buildWhereNotExistsConditions($condition)
    {
        $condition = array_shift($condition);

        if(is_string($condition) && !empty(trim($condition))) {
            return "NOT EXISTS (" . $condition . ')';
        }
    }

    /** Build 'WHERE IS NULL / OR WHERE IS NULL' all conditions. */
    public function buildWhereNullConditions($args)
    {
        $key = array_shift($args);

        if(is_string($key) && !empty(trim($key))) {
            return "$key IS NULL";
        }
    }

    /** Build 'WHERE IS NOT NULL / OR WHERE IS NOT NULL' all conditions. */
    public function buildWhereNotNullConditions($args)
    {
        $key = array_shift($args);

        if(is_string($key) && !empty(trim($key))) {
            return "$key IS NOT NULL";
        }
    }

    /** Concat more conditions to simple string. */
    private function concatConditions($condition, $separator)
    {
        return $condition && substr($condition, -1) !== '(' ? " $separator " : '';
    }
}