<?php

namespace Vendor\DB\Drivers\MongoDB;

use Vendor\DB\Models;

class MongoDB
{
    /** Declare global array about model. */
    public $model;

    /** Declare client class for work with DB. */
    public $clients;

    public function __construct($model)
    {
        // Declare all objects from driver.
        $this->model = $model;

        // Connect to MongoDB.
        $this->clients = $this->connectClient();

        // Declare database.
        $database_name = $this->getDatabaseName();
        $this->clients->database = $this->clients->database->{$database_name};

        // Declare collection.
        $collection_name = $this->getCollectionName();
        $this->clients->collection = $this->clients->collection->{$collection_name};
    }

    /** Connect to MongoDB. */
    private function connectClient()
    {
        return new \MongoDB\Client(DB_MONGODB_LOCALHOST);
    }

    /** Get database name as @singleton if use to many databases. */
    private function getDatabaseName()
    {
        return $this->model->database ?: DB_MONGODB_DATABASE;
    }

    /** Get collection name as @singleton. */
    private function getCollectionName()
    {
        return $this->model->collection ?: (new Models)->getTableName($this->model->model_name);
    }

    /** Declare call method for the working with DB. */
    public function query()
    {
        return $this;
    }
}