<?php

namespace Vendor\DB;

class Models
{
    /** Declare table name. */
    public $table_name;

    /** Declare model name which called. */
    public $model_name;

    /** Declare driver for working with DB. */
    static $driver;

    /** Declare unique model driver for working with DB. */
    public $connection;

    public function __construct()
    {
        $this->model_name = $this->getModelName();

        $this->table_name = $this->getTableName($this->model_name, $this->table_name);
    }

    /** Magic method for call as dynamic style. */
    public function __call($name, array $args)
    {
        return $this->call($name, $args) ?: $this;
    }

    /** Magic method for call as static style. */
    static function __callStatic($name, array $args)
    {
        return (new static())->call($name, $args) ?: new static();
    }

    /** Replace string to call method name which use in model facade. */
    private function call($name, array $args)
    {
        // Get Drivers.
        self::$driver = KERNEL['db'][$this->connection] ?: KERNEL['db'][DB_DRIVER];

        // Call driver.
        if(self::$driver) {
            $class = new self::$driver($this);

            return $class->{$name}($args);
        }
    }

    /** Get called class method of the model for the using in DB request. */
    public function getTableName($model_name, $table_name = null)
    {
        if(!$table_name) {
            $table_name = end( explode('\\', $model_name));
        }

        return strtolower($table_name);
    }

    /** Get called class method of the model for the using in DB request. */
    public function getModelName()
    {
        return get_called_class();
    }
}