<?php

namespace Vendor\Sending;

class Email
{
    public function send($email, $text_msg, $text, $files = null){
        try{

            if(isset($email) && !empty(trim($email))){
                // Get data.
                $header_msg = EMAIL_HEADER;
                $login      = EMAIL_LOGIN;
                $password   = EMAIL_PASSWORD;

                // Build start header for message.
                $header = "Date: ".date("D, j M Y G:i:s")." +0700\r\n";
                $header .= "From: =?UTF-8?Q?".str_replace("+","_",str_replace("%","=",urlencode($header_msg)))."?= <$login>\r\n";
                $header .= "X-Mailer: Test script hosting Ukraine.com.ua \r\n";
                $header .= "Reply-To: =?UTF-8?Q?".str_replace("+","_",str_replace("%","=",urlencode($header_msg)))."?= <$login>\r\n";
                $header .= "X-Priority: 1 (High)\r\n";
                $header .= "Message-ID: <12345654321.".date("YmjHis")."@ukraine.com.ua>\r\n";
                $header .= "To: =?UTF-8?Q?".str_replace("+","_",str_replace("%","=",urlencode('Отримувач ')))."?= <$email>\r\n";
                $header .= "Subject: =?UTF-8?Q?".str_replace("+","_",str_replace("%","=",urlencode($text)))."?=\r\n";
                $header .= "MIME-Version: 1.0\r\n";

                // Send file if attach him.
                if(isset($files) && !empty($files) && $files) {
                    // Generate unique id.
                    $uid = md5(uniqid(time()));

                    $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
                    $header .= "This is a multi-part message in MIME format.\r\n";

                    // Build in header text message.
                    $header .= "--" . $uid . "\r\n";
                    $header .= "Content-Type: text/html; charset=UTF-8\r\n";
                    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
                    $header .= $text_msg . "\r\n\r\n";

                    foreach ($files as $file) {
                        if ($file) {
                            // Get file and build content to send message.
                            $filename = $file['name'];
                            $file = file_get_contents($file['tmp_name']);
                            $content = chunk_split(base64_encode($file));

                            // Build header attach file.
                            $header .= "--" . $uid . "\r\n";
                            $header .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n"; // use different content types here
                            $header .= "Content-Transfer-Encoding: base64\r\n";
                            $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
                            $header .= $content . "\r\n\r\n";
                        }
                    }

                    $header .= "--" . $uid . "--";
                } else {

                    $header.="Content-Type: text/html; charset=UTF-8\r\n";
                    $header.="Content-Transfer-Encoding: 8bit\r\n";

                }

                // Connect to service send message.
                $smtp_conn = fsockopen("mail.ukraine.com.ua", 2525,$errno, $errstr, 10); //соединяемся с почтовым сервером mail.ukraine.com.ua , порт 25 .

                if(!$smtp_conn) {/*print "соединение с серверов не прошло";*/ fclose($smtp_conn); }
                $data = $this->getData($smtp_conn);
                fputs($smtp_conn,"EHLO mail.ukraine.com.ua\r\n"); // начинаем приветствие.
                $code = substr($this->getData($smtp_conn),0,3); // проверяем, не возвратил ли сервер ошибку.
                if($code != 250) {/*print "ошибка приветсвия EHLO";*/ fclose($smtp_conn); }
                fputs($smtp_conn,"AUTH LOGIN\r\n"); // начинаем процедуру авторизации.
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 334) {/*print "сервер не разрешил начать авторизацию";*/ fclose($smtp_conn); }

                fputs($smtp_conn,base64_encode("$login")."\r\n"); // отправляем серверу логин от почтового ящика (на хостинге "Украина" он совпадает с именем почтового ящика).
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 334) {/*print "ошибка доступа к такому юзеру";*/ fclose($smtp_conn); }

                fputs($smtp_conn,base64_encode("$password")."\r\n");       // отправляем серверу пароль.
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 235) {/*print "неправильный пароль";*/ fclose($smtp_conn); }

                fputs($smtp_conn,"MAIL FROM:$login\r\n"); // отправляем серверу значение MAIL FROM.
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 250) {/*print "сервер отказал в команде MAIL FROM";*/ fclose($smtp_conn); }

                fputs($smtp_conn,"RCPT TO:$email\r\n"); // отправляем серверу адрес получателя.
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 250 AND $code != 251) {/*print "Сервер не принял команду RCPT TO";*/ fclose($smtp_conn); }

                fputs($smtp_conn,"DATA\r\n"); // отправляем команду DATA.
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 354) {/*print "сервер не принял DATA"; */ fclose($smtp_conn); }

                fputs($smtp_conn,$header."\r\n".$text_msg."\r\n.\r\n"); // отправляем тело письма.
                $code = substr($this->getData($smtp_conn),0,3);
                if($code != 250) {/*print "ошибка отправки письма";*/ fclose($smtp_conn); }

                fputs($smtp_conn,"QUIT\r\n");   // завершаем отправку командой QUIT.
                fclose($smtp_conn); // закрываем соединение.

                if($code == 250)
                    return 'SEND';
                return 'ERROR';
            }

        }catch (\Exception $errors){

            // Send to E-Mail exception errors.
//            $this->sendToEmailErrors($errors);

            // Return status error.
            return 'ERROR';

        }
    }

    /**
     * Ancillary function for request.
     *
     * @param $smtp_conn
     * @return string
     */
    public function getData($smtp_conn)  // функция получения кода ответа сервера.
    {
        $data="";
        while($str = fgets($smtp_conn,515))
        {
            $data .= $str;
            if(substr($str,3,1) == " ") { break; }
        }
        return $data;
    }
}
