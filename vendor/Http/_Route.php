<?php

use \Vendor\Http\Request;

class Route
{
    // Declare url for next load routes and unset lang-parts from URL.
    static $url;

    // Declare global in group routing url for next load routes and unset lang-parts from URL.
    static $original_url;

    // Declare global class name for else run error 404.
    static $class_name;

    // Declare arguments which will sending to method of the class.
    static $args = [];

    // Declare route name active action.
    static $route_name;

    /** Get URL if declare or fisrt declare if didn't declared him, when @singleton. */
    private function getUrl()
    {
        return self::$url ?: self::$url = (new Request)->server('QUERY_STRING');
    }

    /** Processing version having prefix parameters in group. */
    private function groupPrefixProcessing($callback, $parameters)
    {
        // Get url path and build array exception 0.
        $url = (new static)->filterUrlPath((new static)->getUrl());

        // Get routes path and build array exception 0.
        $routes = (new static)->filterUrlPath($parameters['prefix']);

        // Each to URL path for the searching route.
        $search_url_route = (new static)->searchUrlRoute($url, $routes);

        if(count($url) === $search_url_route && $search_url_route === count($routes)) {
            self::$original_url = self::$url;

            // Remove from URL lot prefix.
            for ($i = 1; $i <= $search_url_route; $i++) {
                unset($url[$i]);
            }

            // Build local url for the children group.
            self::$url = '/' . implode('/', $url);

            // Call callback group.
            call_user_func_array($callback, self::$args ?: []);

            // Returned previous URL.
            self::$url = self::$original_url;
        }
    }

    /** Processing version having prefix parameters in group. */
    private function groupMiddlewareProcessing($parameters)
    {
        if(is_array($parameters['middleware'])) {

            // Recurse all name middleware.
            foreach ($parameters['middleware'] as $middleware) {
                $valid = $this->callMiddleware(KERNEL['middleware'][$middleware], new Request);

                if(!$valid) return true;
            }

        } else {

            // Call name middleware.
            $valid = $this->callMiddleware(KERNEL['middleware'][$parameters['middleware']], new Request);

            if(!$valid) return true;

        }
    }

    /** Call all middleware in alias. */
    private function callMiddleware($middleware_class, &$request)
    {
        if(is_array($middleware_class)) {

            // Recurse all class middleware.
            foreach ($middleware_class as $middleware) {
                $valid = $this->callMiddleware($middleware, $request);

                if(!$valid) return false;
            }

            return true;

        } else if(is_string($middleware_class)) {

            // Call middleware.
            return (boolean) (new $middleware_class)->handle($request);

        }
    }

    /** Handle route and get active. */
    private function handle($routes, $call, $parameters = null)
    {
        // Get url path and build array exception 0.
        $url = (new static)->filterUrlPath((new static)->getUrl());

        // Get routes path and build array exception 0.
        $routes = (new static)->filterUrlPath($routes);

        // Filter another path routes.
        if((count($url) === count($routes))) {

            // Each to URL path for the searching route.
            $search_url_route = (new static)->searchUrlRoute($url, $routes);

            // If count url parts === count static parts from routes.
            if(count($url) === $search_url_route && $search_url_route === count($routes)) {

                // Set name alias in route.
                self::$route_name = $parameters['name'];

                // If is callable route.
                if (is_callable($call)) {
                    return call_user_func_array($call, self::$args ?: []);
                }

                // Get controller and method name.
                $call = explode('@', $call);

                // Include controller.
                require_once (__DIR__ . '/../../App/Controllers/' . $call[0] . '.php');

                // Declare using class name.
                $declare_classes = get_declared_classes();
                self::$class_name = end($declare_classes);

                // Call controller having class and method names.
                return (new static)->callController(
                    self::$class_name,
                    $call[1]
                );

            }
        }
    }

    /** Filter url path. */
    private function filterUrlPath($path)
    {
        return array_filter(explode('/', $path), 'strlen');
    }

    /** Each to URL path for the searching route. */
    private function searchUrlRoute($url, $routes)
    {
        // Declare start increment lot of URL.
        $lot_of_url = 0;

        // Clear arguments.
        self::$args = null;

        // Each from URL for the search concrete route.
        foreach($url as $key => $value) {
            if(isset($routes[$key])) {
                // Find in routes parameters.
                preg_match('/\{(.+)\}/', $routes[$key], $parameter);

                // Get parameters in routes.
                if($parameter) { // if($parameter && $value){
                    self::$args[$parameter[1]] = $value;
                    $lot_of_url++;
                } else if($routes[$key] == $value) {
                    $lot_of_url++;
                }
            }
        }

        return $lot_of_url;
    }

    /** Call controller having class and method names. Use reflection. */
    public function callController($class_name, $method_name)
    {
        // Declare reflection method.
        $reflection = new \ReflectionMethod($class_name, $method_name);

        // Each arguments method and send himself their.
        $send_args = [];
        foreach($reflection->getParameters() as $parameter) {
            $arg = $parameter->getClass();

            // TODO.
//            foreach ($arg->getConstructor()->getParameters() as $a) {
//                dump($a->getClass());
//            }
//            dd();

            // If arguments instance of himself class push to array all arguments.
            if ($arg instanceof ReflectionClass) {
                $arg_class_name = $arg->getName();
                $send_args[] = class_exists($arg_class_name) ? new $arg_class_name : $arg_class_name;
            }
        }

        // Call methods with all arguments.
        return $reflection->invokeArgs(new $class_name, array_merge($send_args));
    }

    /** Get GET-request and route him from application. */
    static function get($routes, $call, $parameters = null)
    {
        if($_SERVER['REQUEST_METHOD'] === 'GET') {
            (new static)->handle($routes, $call, $parameters);
        }
    }

    /** Get POST-request and route him from application. */
    static function post($routes, $call, $parameters = null)
    {
        if($_SERVER['REQUEST_METHOD'] === 'POST') {
            (new static)->handle($routes, $call, $parameters);
        }
    }

    /** Group routes-wrapper for the group application. */
    static function group(array $parameters, $callback)
    {
        if(is_callable($callback)) {
            Request::$arguments = self::$args;

            // If isset middleware parameters in group.
            if(isset($parameters['middleware'])) {
                // If don't valide middleware return group.
                $dont_valide = (new static)->groupMiddlewareProcessing($parameters);
                if($dont_valide) return;
            }

            // If isset prefix parameters in group.
            if(isset($parameters['prefix'])) {
                (new static)->groupPrefixProcessing($callback, $parameters);
            }

            // Default callable callback.
            if(empty($parameters)) {
                call_user_func_array($callback, []);
            }
        }
    }

    /** Declare error method exception from application. */
    static function error($routes, $call)
    {
        switch($routes){
            case '404':
                if(!self::$class_name) {
                    // Get controller and method name.
                    $call = explode('@', $call);

                    // Iclude controller.
                    require_once (__DIR__ . '/../../App/Controllers/' . $call[0] . '.php');

                    // Declare class name.
                    $class_name = end(get_declared_classes());

                    // Start project if this class name isset
                    return (new $class_name)->{$call[1]}();
                }
        }
    }
}