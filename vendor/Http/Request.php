<?php

namespace Vendor\Http;

use Vendor\Errors\Error;
use Vendor\Languages\Translate;

class Request
{
    // Declare arguments in GET request.
    public $get;

    // Declare arguments in POST request.
    public $post;

    /** Declare all parameters. */
    public function __construct()
    {
        $this->all();
    }

    /** Declare parameters GET-request. */
    public function get()
    {
        // Declare new get object.
        $this->get = new \stdClass();

        // Set new arguments.
        $this->get->url = \Route::$url;
        $this->get->route_name = \Route::$route_name;

        // Validate and set new parameters.
        if(\Route::$args) {
            foreach (\Route::$args as $args) {
                $this->get->arguments[] = str_replace('/', '', $args);
            }
        }
    }

    /** Declare parameters POST-request. */
    public function post()
    {
        // Declare new post object.
        $this->post = new \stdClass();

        // Validate and set new parameters.
        if($_POST) {
            // Filter array from injection and trim.
            $this->post = (object) $this->filterRequest($_POST);

            // If request not validate to CSRF attack abort error.
            if(!isset($this->post->{'_csrf-token'}) || $this->post->{'_csrf-token'} !== CSRF_TOKEN) {
                Error::abort(Translate::trans('errors', '_csrf-token'));
            }
        }
    }

    /** Check on POST request. */
    static function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    /** Check on GET request. */
    static function isGet()
    {
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    /** Check on AJAX request. */
    static function isAjax()
    {
        return strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
    }

    /** Get method from request. */
    static function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /** Filter array from injection and trim. */
    public function filterRequest($request)
    {
        // Recursive clear array.
        array_walk_recursive($request, function (&$parameter) {
            $parameter = htmlspecialchars(trim($parameter), ENT_QUOTES, 'UTF-8');
        });

        return $request;
    }

    /** Get global array in magic const. */
    public function server($key = null)
    {
       return $key ? $_SERVER[$key] : $_SERVER;
    }

    /** Declare all parameters. */
    public function all()
    {
        // Get GET-request parameters.
        $this->get();

        // Get POST-request parameters.
        $this->post();

        return $this;
    }
}