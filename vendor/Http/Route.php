<?php

use \Vendor\Http\Request;

class Route
{
    // Declare url for next load routes and unset lang-parts from URL.
    static $url;

    // Declare global in group routing url for next load routes and unset lang-parts from URL.
    static $original_url;

    // Declare exists and valid route.
    static $exists_route = false;

    // Declare arguments which will sending to method of the class.
    static $args = [];

    // Declare route name active action.
    static $route_name;

    // Declare default type patterns for the routes.
    static $default_type_route = 'BRACES';

    // Declare patterns for the preg replaces.
    static $route_patterns = [
        'PATTERN' => [
            // Declare {id} or {id?} type pattern.
            '/\\\{[a-zA-Z0-9\_\-]+\\\}/',
            '/\\/\\\\\{[a-zA-Z0-9\_\-]+\\\\\?\\\\\}/',

            // Declare :id or :id? type pattern.
            '/\\/\\\\\:[a-zA-Z0-9\_\-]+\\\\\?/',
            '/\\\:[a-zA-Z0-9\_\-]+/',
        ],
        'REPLACEMENT' => [
            // Declare {id} or {id?} type pattern.
            '([a-zA-Z0-9\-\_]+)',
            '(/[a-zA-Z0-9\-\_]+)?',

            // Declare :id or :id? type pattern.
            '(/[a-zA-Z0-9\-\_]+)?',
            '([a-zA-Z0-9\-\_]+)',
        ]
    ];

    /** Get URL if declare or fisrt declare if didn't declared him, when @singleton. */
    private function getUrl()
    {
        return self::$url ?: self::$url = (new Request)->server('QUERY_STRING');
    }

    /** Call callback funtion if valid route and callback is callable. */
    private function callCallback($callback)
    {
        $request = new Request();
        return call_user_func($callback, $request->get->arguments);
    }

    /** Handle RESTFull requests: [PUT, PATCH, DELETE, HEAD, OPTIONS]. */
    private function restRoutes($method_name, $routes, $call, $parameters)
    {
        // Get method name route.
        $request = new Request();
        $request_method = $_SERVER['REQUEST_METHOD'];
        $method_name = strtoupper($method_name);

        // Validate route.
        if($request_method === $method_name || ($request_method === 'POST' && is_string($request->post->{'_method'}) && strtoupper($request->post->{'_method'}) === $request_method)) {
            // Declare alias active route.
            self::$route_name = $parameters['name'] ?: null;

            return (new static)->handle($routes, $call);
        }
    }

    /** Processing version having prefix parameters in group. */
    private function groupPrefixProcessing($parameters)
    {
        // Get url path and build array exception 0.
        $url = (new static)->getUrl();

        // Get routes path and build array exception 0.
        $routes = $parameters['prefix'];

        // Handle route to valid.
        $do_route = (new static)->doRoute($url, $routes, true);

        // If valid route.
        if(is_array($do_route)) {
            // Get prefix url and change child group route url.
            $prefix_url = array_shift($do_route);

            // Change child group URL.
            self::$original_url = self::$url;
            self::$url = str_replace($prefix_url, '', self::$url);

            // Declare arguments.
            self::$args = $do_route;

            return true;
        }

        return false;
    }

    /** Processing version having prefix parameters in group. */
    private function groupMiddlewareProcessing($parameters)
    {
        if(is_array($parameters['middleware'])) {

            // Recurse all name middleware.
            foreach ($parameters['middleware'] as $middleware) {
                $valid = $this->callMiddleware(KERNEL['middleware'][$middleware], new Request);

                if(!$valid) return true;
            }

        } else {

            // Call name middleware.
            $valid = $this->callMiddleware(KERNEL['middleware'][$parameters['middleware']], new Request);

            if(!$valid) return true;

        }
    }

    /** Call all middleware in alias. */
    private function callMiddleware($middleware_class, &$request)
    {
        if(is_array($middleware_class)) {

            // Recurse all class middleware.
            foreach ($middleware_class as $middleware) {
                $valid = $this->callMiddleware($middleware, $request);

                if(!$valid) return false;
            }

            return true;

        } else if(is_string($middleware_class)) {

            // Call middleware.
            return (boolean) (new $middleware_class)->handle($request);

        }
    }

    /** Handle route and get active. */
    private function handle($routes, $call)
    {
        // Get url path and build array exception 0.
        $url = (new static)->getUrl();

        // Handle route to valid.
        $do_route = (new static)->doRoute($url, $routes, true);

        // If valid route.
        if(is_array($do_route)) {
            // Get arguments.
            unset($do_route[0]);
            self::$args = $do_route;

            // If is callable route.
            if (is_callable($call)) {
                return $this->callCallback($call);
            }

            // Load controller and set to static name controller.
            $call = $this->loadController($call);

            // Call controller having class and method names.
            return $this->callController($call);
        }
    }

    /** Each to URL path for the searching route. */
    private function doRoute($url, $routes, $prefix = false)
    {
        // Build route pattern. Convert urls like '/:id/{id}/id?/{id?}' to regular expression.
        $pattern_replace = preg_replace(self::$route_patterns['PATTERN'], self::$route_patterns['REPLACEMENT'], preg_quote($routes));

        // Build result route pattern.
        $pattern = '@^' . $pattern_replace . ($prefix ? '' : '$') .'@D';

        // Check if the current request matches the expression.
        if(preg_match($pattern, $url, $matches)) {
            return $matches;
        }

        return false;
    }

    /** Get all parameters
    in route. */
    private function getParametersRoute($parameters)
    {
        // Get prefix in request and remove in URL it.
        if(isset($parameters['prefix'])) {
            if(!(new static)->groupPrefixProcessing($parameters)) {
                return false;
            }
        }

        // If isset middleware parameters in group. If don't valid middleware return group.
        if(isset($parameters['middleware'])) {
            if((new static)->groupMiddlewareProcessing($parameters)) {
                return false;
            }
        }

        // Set name alias in route.
        if(isset($parameters['name'])) {
            self::$route_name = $parameters['name'];
        }

        return true;
    }

    /** Load controller having class and method names. Return class and method name. */
    private function loadController($call)
    {
        // Get controller and method name.
        $call = explode('@', $call);

        // Include controller.
        return [
            'class_name' => 'App\\Controllers\\' . $call[0],
            'method_name' => $call[1],
        ];
    }

    /** Call controller having class and method names. Use reflection. */
    public function callController($call)
    {
        // Declare reflection method.
        $reflection = new \ReflectionMethod($call['class_name'], $call['method_name']);

        // Each arguments method and send himself their.
        $send_args = [];
        foreach($reflection->getParameters() as $parameter) {
            $arg = $parameter->getClass();

            // TODO.
//            foreach ($arg->getConstructor()->getParameters() as $a) {
//                dump($a->getClass());
//            }
//            dd();

            // If arguments instance of himself class push to array all arguments.
            if ($arg instanceof ReflectionClass) {
                $arg_class_name = $arg->getName();
                $send_args[] = class_exists($arg_class_name) ? new $arg_class_name : $arg_class_name;
            }
        }

        // Call methods with all arguments.
        self::$exists_route = $reflection->invokeArgs(new $call['class_name'], array_merge($send_args));
        return self::$exists_route;
    }

    /** Get GET-request and route him from application. */
    static function get($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Get POST-request and route him from application. */
    static function post($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Get PUT-request and route him from application. */
    static function put($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Get PATCH-request and route him from application. */
    static function patch($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Get DELETE-request and route him from application. */
    static function delete($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Get HEAD-request and route him from application. */
    static function head($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Get OPTIONS-request and route him from application. */
    static function options($routes, $call, $parameters = null)
    {
        return (new static())->restRoutes(__FUNCTION__, $routes, $call, $parameters);
    }

    /** Group routes-wrapper for the group application. */
    static function group(array $parameters, $callback)
    {
        if(is_callable($callback)) {
            // If valid request call callback function.
            if(!(new static)->getParametersRoute($parameters)) {
                return;
            } else {
                (new static())->callCallback($callback);
            }

            // Returned previous URL.
            self::$url = self::$original_url ?: self::$url;
        }
    }

    /** Declare error method exception from application. */
    static function error($routes, $call)
    {
        switch($routes){
            case '404':
                if(!self::$exists_route) {
                    // Load controller and set to static name controller.
                    $call = (new static)->loadController($call);

                    // Start project if this class name isset
                    return (new static)->callController($call);
                }
        }
    }
}