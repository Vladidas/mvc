<?php

class Console
{
    /** Declare method which detected console calling. */
    static function call($arg, $call)
    {
        // Get arguments which send in console.
        $call_args = $GLOBALS['argv'];

        // Find active command.
        if($call_args[1] === $arg) {
            // If is callable route.
            if (is_callable($call)) {
                return call_user_func_array($call, $call_args ?: []);
            }

            // Get controller and method name.
            $call = explode('@', $call);

            // Include controller.
            require_once (__DIR__ . '/../../App/Controllers/' . $call[0] . '.php');

            // Declare using class name.
            $declare_classes = get_declared_classes();
            $class_name = end($declare_classes);

            // Call controller having class and method names.
            return (new Route)->callController(
                $class_name,
                $call[1]
            );
        }
    }
}