<?php

namespace Vendor\Http;

class Redirect
{
    /** Redirect to URL */
    static function to($path)
    {
        die(header('Location:' . $path));
    }

    /** Redirect to back */
    static function back()
    {
        die(header('Location:' . ($_SERVER['HTTP_REFERER'])));
    }
}
