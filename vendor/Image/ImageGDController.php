<?php

namespace Vendor\Image;

class ImageGDController
{
    /** Resize image and don`t stretch him. */
    static function resize($old_tmp_file, $new_file, $w, $h, $quality = 100)
    {
        // If empty filename.
        if(!$old_tmp_file) {
            return false;
        }

        // Get original image parameters.
        list($source_width, $source_height, $source_type) = getimagesize($old_tmp_file);

        // Create fily type palete.
        switch ($source_type) {
            case IMAGETYPE_GIF:
                $source_gdim = imagecreatefromgif($old_tmp_file);
                break;
            case IMAGETYPE_JPEG:
                $source_gdim = imagecreatefromjpeg($old_tmp_file);
                break;
            case IMAGETYPE_PNG:
                $source_gdim = imagecreatefrompng($old_tmp_file);
                break;
            default:
                $source_gdim = imagecreatefromjpeg($old_tmp_file);
                break;
        }

        if($w && !$h) {

            // Save to fixed width.
            if($w >= $source_width) {
                $new_w = $source_width;
                $new_h = $source_height;
            } else {
                $new_w = $w;
                $new_h = round($source_height * $w / $source_width);
            }

            // Create layout image.
            $temp_gdim = imagecreatetruecolor($new_w, $new_h);

        } else if(!$w && $h) {

            // Save to fixed width.
            if($h >= $source_height) {
                $new_h = $source_height;
                $new_w = $source_width;
            } else {
                $new_h = $h;
                $new_w = round($source_width * $h / $source_height);
            }

            // Create layout image.
            $temp_gdim = imagecreatetruecolor($new_w, $new_h);

        } else if(!$w && !$h) {

            // Save original image.
            $new_w = $source_width;
            $new_h = $source_height;

            // Create layout image.
            $temp_gdim = imagecreatetruecolor($new_w, $new_h);

        } else {

            // Save to fixed width and height.
            // Cropping image to fixed size.
            $temp_gdim = self::cropImage($source_gdim, $source_width, $source_height, $w, $h);
        }

        imagealphablending( $temp_gdim, false );
        imagesavealpha( $temp_gdim, true );

        // Save image.
        imagecopyresampled(
            $temp_gdim,
            $source_gdim,
            0, 0,
            0, 0,
            $new_w, $new_h,
            $source_width, $source_height
        );

        // Create fily type palete.
        switch ($source_type) {
            case IMAGETYPE_PNG:
                $png_quality = ($quality - 100) / 11.111111;
                $png_quality = round(abs($png_quality));
                return imagepng($temp_gdim, $new_file, $png_quality);

            default:
                return imagejpeg($temp_gdim, $new_file, $quality);
        }

//        header('Content-type: image/jpeg');
    }

    /** Asistent method cropping image to fixed size. */
    static function cropImage($source_gdim, $source_width, $source_height, $w, $h)
    {
        // Get ratio of the image.
        $source_aspect_ratio = $source_width / $source_height;
        $desired_aspect_ratio = $w / $h;

        if ($source_aspect_ratio > $desired_aspect_ratio) {
            // Triggered when source image is wider.
            $temp_height = $h;
            $temp_width = (int) ($h * $source_aspect_ratio);
        } else {
            // Triggered otherwise (i.e. source image is similar or taller).
            $temp_width = $w;
            $temp_height = (int) ($w / $source_aspect_ratio);
        }

        // Resize the image into a temporary GD image.
        $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
        imagecopyresampled(
            $temp_gdim,
            $source_gdim,
            0, 0,
            0, 0,
            $temp_width, $temp_height,
            $source_width, $source_height
        );

        // Copy cropped region from temporary image into the desired GD image.
        $x0 = ($temp_width - $w) / 2;
        $y0 = ($temp_height - $h) / 2;
        $desired_gdim = imagecreatetruecolor($w, $h);
        imagecopy(
            $desired_gdim,
            $temp_gdim,
            0, 0,
            $x0, $y0,
            $w, $h
        );

        // Save file to result folder.
        return $desired_gdim;
    }

    /** Cropping image and don`t stretch him. */
    static function crop($old_file, $new_file, $w, $h, $quality = 100)
    {
        if(!empty($old_file)){
            // Get all files.
            list($source_width, $source_height, $source_type) = getimagesize($old_file);

            // Get extension of the file.
            switch ($source_type) {
                case IMAGETYPE_GIF:
                    $source_gdim = imagecreatefromgif($old_file);
                    break;
                case IMAGETYPE_JPEG:
                    $source_gdim = imagecreatefromjpeg($old_file);
                    break;
                case IMAGETYPE_PNG:
                    $source_gdim = imagecreatefrompng($old_file);
                    break;
                default:
                    $source_gdim = imagecreatefromjpeg($old_file);
                    break;
            }

            // Cropping image to fixed size.
            $desired_gdim = self::cropImage($source_gdim, $source_width, $source_height, $w, $h);

            // Save file to result folder.
            imagejpeg($desired_gdim, $new_file, $quality);
        }
    }

    /** Resize image and don`t stretch him. */
    static function saveBase64Resize($old_file, $new_file, $w, $h, $quality = 100)
    {
        $exploded = explode(',', $old_file, 2); // limit to 2 parts, i.e: find the first comma
        $encoded = $exploded[1]; // pick up the 2nd part
        $decoded = base64_decode($encoded);
        $source_gdim = imagecreatefromstring($decoded);

        //saving the image into memory (for manipulation with GD Library)
        $source_width = imagesx($source_gdim);
        $source_height = imagesy($source_gdim);

        // Set default values.
        $new_w = $w;
        $new_h = $h;

        if($w && !$h) {
                // Save to fixed width.
                if($w >= $source_width) {
                    $new_w = $source_width;
                    $new_h = $source_height;
                } else {
                    $new_w = $w;
                    $new_h = round($source_height * $w / $source_width);
                }
        } if(!$w && $h) {
            // Save to fixed width.
            if($h >= $source_height) {
                $new_h = $source_height;
                $new_w = $source_width;
            } else {
                $new_h = $h;
                $new_w = round($source_width * $h / $source_height);
            }
        } else if(!$w && !$h) {
            // Save original image.
            $new_w = $source_width;
            $new_h = $source_height;
        }

        // copying the part into thumbnail
        $temp_gdim = imagecreatetruecolor($new_w, $new_h);
        imagecopyresampled(
            $temp_gdim,
            $source_gdim,
            0, 0,
            0, 0,
            $new_w, $new_h,
            $source_width, $source_height
        );

        return imagejpeg($temp_gdim, $new_file, $quality);
    }

//    static function saveBase64Crop($old_file, $new_file, $w, $h)
//    {
//        $exploded = explode(',', $old_file, 2); // limit to 2 parts, i.e: find the first comma
//        $encoded = $exploded[1]; // pick up the 2nd part
//        $decoded = base64_decode($encoded);
//        $myImage = imagecreatefromstring($decoded);
//
//        //saving the image into memory (for manipulation with GD Library)
//        $width = imagesx($myImage);
//        $height = imagesy($myImage);
//
//// calculating the part of the image to use for thumbnail
//        if ($width > $height) {
//            $y = 0;
//            $x = ($width - $height) / 2;
//            $smallestSide = $height;
//        } else {
//            $x = 0;
//            $y = ($height - $width) / 2;
//            $smallestSide = $width;
//        }
//
//// copying the part into thumbnail
//        $thumbSize = 100;
//        $image_little = imagecreatetruecolor($thumbSize, $thumbSize);
//        imagecopyresampled($image_little, $myImage, 0, 0, $x, $y, $thumbSize, $thumbSize, $smallestSide, $smallestSide);
//
//        return imagejpeg($image_little, $new_file, 100);
//    }
}
