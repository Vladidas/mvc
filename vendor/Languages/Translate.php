<?php

namespace Vendor\Languages;

class Translate
{
    static $lang_id;

    /** Get active language as @singleton. */
    private function getLanguage()
    {
        return self::$lang_id ?: self::$lang_id = $GLOBALS['LANGUAGES'][0];
    }

    /** Declare to translate to selected languages. */
    static function getFile($filename, $lang_id = null)
    {
        // Get active language.
        $lang_id = (new self)->getLanguage();

        return require_once ("{__DIR__}/../App/Views/lang/{$lang_id}/{$filename}.php");
    }

    /** Get translate value. */
    static function trans($filename, $key)
    {
        // Get active language.
        $lang_id = (new self)->getLanguage();

        // Get all values translation.
        $translations = require ("{__DIR__}/../App/Views/lang/{$lang_id}/{$filename}.php");

        return $translations[$key];
    }
}
