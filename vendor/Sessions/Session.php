<?php

namespace Vendor\Sessions;

class Session
{
    // Send and get once session from exchange data.
    static function flash($key, $value = null)
    {
       // If get session.
       if(is_null($value)){
           if(isset($_SESSION['flash'][$key])){
               // Send data from session to variable.
               $value = $_SESSION['flash'][$key];

               // Remove from session data.
               unset($_SESSION['flash'][$key]);

               // Send flash-data.
               return $value;
           }

           // Default flash-data.
           return false;
       }

       // Save to session data.
       $_SESSION['flash'][$key] = $value;
    }
}