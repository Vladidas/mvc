<?php

namespace Vendor\Errors;

use Vendor\Sessions\Session;

class Error
{
    /** Aborting error. */
    static function abort($exception)
    {
        try {
            throw new \Exception($exception);
        }catch (\Exception $exception) {
            self::realizeErrors($exception);
            die;
        }
    }

    /** Show/print errors. */
    static function realizeErrors($exception)
    {
        // Show errors.
        if(ERRORS_LOGS_SHOW_ENABLE) {
            self::exceptionToShow($exception);
        }

        // Print to file.
        if(ERRORS_LOGS_FILE_ENABLE) {
            self::exceptionToFile($exception);
        }
    }

    /** Exception to show errors log. */
    static function exceptionToShow($exception)
    {
        // Get data exceptions.
        $message = self::textError($exception);

        echo nl2br($message);
    }

    /** Exception to file errors log. */
    static function exceptionToFile($exception)
    {
        // Get data exceptions.
        $message = self::textError($exception);

        // Save to file one time errors logs.
        if($message !== Session::flash('errors_log')) {
            Session::flash('errors_log', $message);
            error_log($message);
        }
    }

    /** Building text message error. */
    static function textError($exception)
    {
        $message = 'ERROR: ' . date('d.m.Y H:m:i') . PHP_EOL . PHP_EOL;
        $message .= 'Message: ' . ($exception->getMessage() ?: '') . PHP_EOL;
        $message .= 'File: ' . ($exception->getFile() ?: '') . PHP_EOL;
        $message .= 'Line: ' . ($exception->getLine() ?: '') . PHP_EOL . PHP_EOL;
        $message .= 'Trace: ' . ($exception->getTraceAsString() ?: '') . PHP_EOL . PHP_EOL;
        $message .= '--------------------------' . PHP_EOL;

        return $message;
    }
}